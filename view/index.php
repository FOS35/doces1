<?php
#iniciar sessão
session_start();

#banco de dados
include '../banco/db.php';

#Rotas
if(isset($_SESSION['login'])){
	if(isset($_GET['pagina'])){
		$pagina = $_GET['pagina'];
	}else{
		include 'header.php';
		include 'aside.php';
		$pagina = 'home';
	}
}else{
	$pagina = 'login';
}
//$pagina = $_GET['pagina'];

switch ($pagina) {
	case 'home':
		include 'home.php';
		break;
	case 'bootstrap-components':
		include 'bootstrap-components.php';
		break;
	case 'ui-cards':
		include 'ui-cards.php';
		break;
	case 'widgets':
		include 'widgets.php';
		break;
	case 'charts':
		include 'charts.php';
		break;
	case 'form-components':
		include 'form-components.php';
		break;
	case 'form-custom':
		include 'form-custom.php';
		break;
	case 'form-samples':
		include 'form-samples.php';
		break;
	case 'form-notifications':
		include 'form-notifications.php';
		break;
	case 'table-basic':
		include 'table-basic.php';
		break;
	case 'table-data-table':
		include 'table-data-table.php';
		break;
	case 'blank-page':
		include 'blank-page.php';
		break;
	case 'page-lockscreen':
		include 'page-lockscreen.php';
		break;
	case 'page-user':
		include 'page-user.php';
		break;
	case 'page-invoice':
		include 'page-invoice.php';
		break;
	case 'page-calendar':
		include 'page-calendar.php';
		break;
	case 'page-mailbox':
		include 'page-mailbox.php';
		break;
	case 'page-error':
		include 'page-error.php';
		break;
	case 'show-franqueados':
		include 'show-franqueados.php';
		break;
	case 'add-produto':
		include 'add-produto.php';
		break;
	case 'add-franqueados':
		include 'add-franqueados.php';
		break;
	case 'show-franqueados':
		include 'show-franqueados.php';
		break;
	case 'imagem':
		include 'imagem.php';
		break;
	case 'salvar_foto':
		include '../actions/salvar_foto.php';
		break;
	case 'page-login':
		include 'page-login.php';
		break;
	case 'show-produtos':
		include 'show-produtos.php';
		break;
	case 'page-login':
		include 'page-login.php';
		break;
	case 'page-login':
		include 'page-login.php';
		break;
	case 'login':
		include 'page-login.php';
		break;
	default:
		include '../actions/logout.php';
		include 'page-login.php';
		break;
}