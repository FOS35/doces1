<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <!-- Twitter meta-->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:site" content="@pratikborsadiya">
    <meta property="twitter:creator" content="@pratikborsadiya">
    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Vali Admin">
    <meta property="og:title" content="Vali - Free Bootstrap 4 admin theme">
    <meta property="og:url" content="http://pratikborsadiya.in/blog/vali-admin">
    <meta property="og:image" content="http://pratikborsadiya.in/blog/vali-admin/hero-social.png">
    <meta property="og:description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <title>Doces CaLe</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <?php
      include 'header.php';
    ?>

    <!-- Sidebar menu-->
    <?php
      include 'aside.php';
    ?>

    <main class="app-content">
      <div class="tile user-settings">
        

          <?php 
          if(!isset($_GET['editar'])){
          ?>
          <div class="row mb-11">
            <div class="col-md-11">
              <a href="index.php?pagina=show-franqueados"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>  
            </div>
          </div>

          <br>

          <h3 class="line-head">Adicionar franqueado</h3>
          <form enctype="multipart/form-data" method="post" action="../actions/cad_franqueados.php">
            <div class="row mb-6">
              <div class="col-md-6">
                <h5>Nome: </h5>
                <input class="form-control" type="text" name="nome" required autofocus>
              </div>
              <div class="col-md-6">
                <h5>Sobrenome: </h5>
                <input class="form-control" type="text"  name="sobrenome" required>
              </div>
            </div>
            <br>
            <div class="row mb-6">
              <div class="col-md-6">
                <h5>Idade: </h5>
                <input type="number" name="idade" class="[ form-control ]" min="16" max="55" placeholder="Idade mínima 16 anos">
              </div>
              <div class="col-md-6">
                <h5>Atribuição: </h5>
                <input class="form-control" type="text" name="atribuicao" required>
              </div>
            </div>
            <br>
            <div class="row mb-6">
              <div class="col-md-6">
                <div>
                  <h5>Status: </h5>  
                </div>
                <div>
                  <div class="animated-radio-button">
                    <label>
                      <input type="radio" name="status" value="Ativo" checked><span class="label-text"><strong>Ativo</strong></span>
                    </label>
                  </div>
                  <div class="animated-radio-button">
                    <label>
                      <input type="radio" name="status" value="Inativo"><span class="label-text"><strong>Inativo</strong></span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <br>
            <div class="row mb-10">
              <div class="col-md-12">
                <input class="btn btn-primary" type="submit" value="Salvar"><i class="fa fa-fw fa-lg fa-check-circle"></i></button>
              </div>
            </div>
          </form>  
        <?php 
        }else{
          while($linha = mysqli_fetch_array($consultar_franqueados)){
            if($linha['id_franqueado'] == $_GET['editar']){
        ?>

          <div class="row mb-11">
            <div class="col-md-11">
              <a href="index.php?pagina=show-franqueados"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>  
            </div>
          </div>

          <br>

          <h3 class="line-head">Editar franqueado</h3>  
          <form enctype="multipart/form-data" method="post" action="../actions/alt_franqueados.php">
            <div class="row mb-6">
              <div class="col-md-6">
                <h5>Nome: </h5>
                <input class="form-control" type="hidden" name="id_franqueado" value="<?php echo $linha['id_franqueado']?>">
                <input class="form-control" type="text" name="nome" value="<?php echo $linha['nome']?>" required autofocus>
              </div>
              <div class="col-md-6">
                <h5>Sobrenome: </h5>
                <input class="form-control" type="text"  name="sobrenome" value="<?php echo $linha['sobrenome']?>" required>
              </div>
            </div>
            <br>
            <div class="row mb-6">
              <div class="col-md-6">
                <h5>Idade: </h5>
                <input type="number" name="idade" class="[ form-control ]" min="16" max="55" placeholder="Idade mínima 16 anos" value="<?php echo $linha['idade']?>" required>
              </div>
              <div class="col-md-6">
                <h5>Atribuição: </h5>
                <input class="form-control" type="text" name="atribuicao" value="<?php echo $linha['atribuicao']?>" required>
              </div>
            </div>
            <br>
            <div class="row mb-6">
              <div class="col-md-6">
                <div>
                  <h5>Status: </h5>  
                </div>
                <div>
                  <div class="animated-radio-button">
                    <?php 

                    ?>
                    <label>
                      <input type="radio" name="sstatus" value="Ativo" <?php if($linha['sstatus'] == 'Ativo'){?> checked <?php }?>><span class="label-text"><strong>Ativo</strong></span>
                    </label>
                  </div>
                  <div class="animated-radio-button">
                    <label>
                      <input type="radio" name="sstatus" value="Inativo"<?php if($linha['sstatus'] == 'Inativo'){?> checked <?php }?>><span class="label-text"><strong>Inativo</strong></span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <br>
            <div class="row mb-10">
              <div class="col-md-12">
                <input class="btn btn-primary" type="submit" value="Salvar"><i class="fa fa-fw fa-lg fa-check-circle"></i></button>
              </div>
            </div>
          </form>
          <?php 
              }
            }
          }
          ?>
            <!--
            <div class="row">
              <div class="col-md-8 mb-4">
                <label>Email</label>
                <input class="form-control" type="text">
              </div>
              <div class="clearfix"></div>
              <div class="col-md-8 mb-4">
                <label>Mobile No</label>
                <input class="form-control" type="text">
              </div>
              <div class="clearfix"></div>
              <div class="col-md-8 mb-4">
                <label>Office Phone</label>
                <input class="form-control" type="text">
              </div>
              <div class="clearfix"></div>
              <div class="col-md-8 mb-4">
                <label>Home Phone</label>
                <input class="form-control" type="text">
              </div>
          </div>
        -->

          
        </form>
      </div>
    </main>
    <!-- Essential javascripts for application to work-->
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="../js/plugins/pace.min.js"></script>
    <!-- Page specific javascripts-->
    <script type="text/javascript" src="../js/plugins/chart.js"></script>
    <script type="text/javascript">
      var data = {
      	labels: ["January", "February", "March", "April", "May"],
      	datasets: [
      		{
      			label: "My First dataset",
      			fillColor: "rgba(220,220,220,0.2)",
      			strokeColor: "rgba(220,220,220,1)",
      			pointColor: "rgba(220,220,220,1)",
      			pointStrokeColor: "#fff",
      			pointHighlightFill: "#fff",
      			pointHighlightStroke: "rgba(220,220,220,1)",
      			data: [65, 59, 80, 81, 56]
      		},
      		{
      			label: "My Second dataset",
      			fillColor: "rgba(151,187,205,0.2)",
      			strokeColor: "rgba(151,187,205,1)",
      			pointColor: "rgba(151,187,205,1)",
      			pointStrokeColor: "#fff",
      			pointHighlightFill: "#fff",
      			pointHighlightStroke: "rgba(151,187,205,1)",
      			data: [28, 48, 40, 19, 86]
      		}
      	]
      };
      var pdata = [
      	{
      		value: 300,
      		color: "#46BFBD",
      		highlight: "#5AD3D1",
      		label: "Complete"
      	},
      	{
      		value: 50,
      		color:"#F7464A",
      		highlight: "#FF5A5E",
      		label: "In-Progress"
      	}
      ]
      
      var ctxl = $("#lineChartDemo").get(0).getContext("2d");
      var lineChart = new Chart(ctxl).Line(data);
      
      var ctxp = $("#pieChartDemo").get(0).getContext("2d");
      var pieChart = new Chart(ctxp).Pie(pdata);
    </script>


    <!-- Google analytics script-->
    <script type="text/javascript">
      if(document.location.hostname == 'pratikborsadiya.in') {
      	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      	ga('create', 'UA-72504830-1', 'auto');
      	ga('send', 'pageview');
      }
    </script>
  </body>
</html>