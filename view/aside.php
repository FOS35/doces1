<?php include '../banco/db.php' ?>
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user">
        <a href="index.php?pagina=imagem">
          <img class="app-sidebar__user-avatar" src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg"  type="file" alt="User Image">
        </a>

        <div>
          <?php
            if(isset($_SESSION['login'])){
          ?>
          <p class="app-sidebar__user-name">
          <?php
              echo $_SESSION['email'];
            }
          ?>
          </p>
          <p class="app-sidebar__user-designation">
          <?php
            if(isset($_SESSION['login'])){
              while($linha = mysqli_fetch_array($consultar_usuarios)){
                if($linha['email'] == $_SESSION['email']){
                  echo $linha['nome'];
          ?>
          </p>
          <?php
                }
              }
            }
          ?>
        </div>
      </div>
      <ul class="app-menu">
        <!--href="home.php"-->
        <li>
          <a class="app-menu__item active" href="index.php?pagina=home">
            <i class="app-menu__icon fa fa-dashboard">
            </i>
            <span class="app-menu__label">
              Início
            </span>
          </a>
        </li>
        <li class="treeview">
          <a class="app-menu__item" href="#" data-toggle="treeview">
            <i class="app-menu__icon fa fa-laptop">
            </i>
            <span class="app-menu__label">
              UI Elements
            </span>
            <i class="treeview-indicator fa fa-angle-right">
            </i>
          </a>
          <ul class="treeview-menu">
            <!--href="bootstrap-components.php"-->
            <li><a class="treeview-item" href="index.php?pagina=bootstrap-components"><i class="icon fa fa-circle-o"></i> Bootstrap Elements</a></li>
            <li><a class="treeview-item" href="https://fontawesome.com/v4.7.0/icons/" target="_blank" rel="noopener"><i class="icon fa fa-circle-o"></i> Font Icons</a></li>
            <!--href="ui-cards.php"-->
            <li><a class="treeview-item" href="index.php?pagina=ui-cards"><i class="icon fa fa-circle-o"></i> Cards</a></li>
            <!--href="widgets.php"-->
            <li><a class="treeview-item" href="index.php?pagina=widgets"><i class="icon fa fa-circle-o"></i> Widgets</a></li>
          </ul>
        </li>
        <!--href="charts.php"-->
        <li><a class="app-menu__item" href="index.php?pagina=charts"><i class="app-menu__icon fa fa-pie-chart"></i><span class="app-menu__label">Charts</span></a></li>
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">Forms</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <!--href="form-components.php"-->
            <li><a class="treeview-item" href="index.php?pagina=form-components"><i class="icon fa fa-circle-o"></i> Form Components</a></li>
            <!--href="form-custom.php"-->
            <li><a class="treeview-item" href="index.php?pagina=form-custom"><i class="icon fa fa-circle-o"></i> Custom Components</a></li>
            <!--href="form-samples.php"-->
            <li><a class="treeview-item" href="index.php?pagina=form-samples"><i class="icon fa fa-circle-o"></i> Form Samples</a></li>
            <!--href="form-notifications.php"-->
            <li><a class="treeview-item" href="index.php?pagina=form-notifications"><i class="icon fa fa-circle-o"></i> Form Notifications</a></li>
          </ul>
        </li>
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-th-list"></i><span class="app-menu__label">Tables</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <!--href="table-basic.php"-->
            <li><a class="treeview-item" href="index.php?pagina=table-basic"><i class="icon fa fa-circle-o"></i> Basic Tables</a></li>
            <!--href="table-data-table.php"-->
            <li><a class="treeview-item" href="index.php?pagina=table-data-table"><i class="icon fa fa-circle-o"></i> Data Tables</a></li>
          </ul>
        </li>
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-file-text"></i><span class="app-menu__label">Pages</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <!--href="blank-page.php"-->
            <li><a class="treeview-item" href="index.php?pagina=blank-page"><i class="icon fa fa-circle-o"></i> Blank Page</a></li>
            <!--href="page-login.php"-->
            <li><a class="treeview-item" href="index.php?pagina=page-login"><i class="icon fa fa-circle-o"></i> Login Page</a></li>
            <!--href="page-lockscreen.php"-->
            <li><a class="treeview-item" href="index.php?pagina=page-lockscreen"><i class="icon fa fa-circle-o"></i> Lockscreen Page</a></li>
            <!--href="page-user.php"-->
            <li><a class="treeview-item" href="index.php?pagina=page-user"><i class="icon fa fa-circle-o"></i> User Page</a></li>
            <!--href="page-invoice.php"-->
            <li><a class="treeview-item" href="index.php?pagina=page-invoice"><i class="icon fa fa-circle-o"></i> Invoice Page</a></li>
            <!--href="page-calendar.php"-->
            <li><a class="treeview-item" href="index.php?pagina=page-calendar"><i class="icon fa fa-circle-o"></i> Calendar Page</a></li>
            <!--href="page-mailbox.php"-->
            <li><a class="treeview-item" href="index.php?pagina=page-mailbox"><i class="icon fa fa-circle-o"></i> Mailbox</a></li>
            <!--href="page-error.php"-->
            <li><a class="treeview-item" href="index.php?pagina=page-error"><i class="icon fa fa-circle-o"></i> Error Page</a></li>
          </ul>
        </li>
      </ul>
    </aside>