<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <!-- Twitter meta-->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:site" content="@pratikborsadiya">
    <meta property="twitter:creator" content="@pratikborsadiya">
    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Vali Admin">
    <meta property="og:title" content="Vali - Free Bootstrap 4 admin theme">
    <meta property="og:url" content="http://pratikborsadiya.in/blog/vali-admin">
    <meta property="og:image" content="http://pratikborsadiya.in/blog/vali-admin/hero-social.png">
    <meta property="og:description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <title>Basic Tables - Vali Admin</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <?php
      include '../banco/db.php';
      include 'header.php';
    ?>
    
    <!-- Sidebar menu-->
    <?php
      include 'aside.php';
    ?>
    
    <main class="app-content">
      <div class="row">
        <div class="clearfix"></div>
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <h3 class="tile-title">Todos os franqueados</h3>

              <div class="row mb-11">
                <div class="col-md-11">
                  <a href="index.php?pagina=home"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>  
                </div>
                <div class="col-md-1">
                  <a href="index.php?pagina=add-franqueados"><i class="fa fa-plus" aria-hidden="true"></i></a>
                </div>
              </div>
              
              <br>
              <div class="table-responsive">
                <table class="table table-hover table-bordered" id="sampleTable">
                  <thead>
                    <tr>
                      <th>Nome</th>
                      <th>Idade</th>
                      <th>Atribuição</th>
                      <th>Status</th>
                      <th>Editar</th>
                      <th>Deletar</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      while($linha = mysqli_fetch_array($consultar_franqueados)){
                        echo '<tr>
                                <td>'
                                  .$linha['nome'].' '.$linha['sobrenome'].
                                '</td>
                                <td>'
                                  .$linha['idade'].
                                '</td>
                                <td>'
                                  .$linha['atribuicao'].
                                '</td>
                                <td>'
                                  .$linha['sstatus'].
                                '</td>';
                    ?>
                    <td>
                      <a href="index.php?pagina=add-franqueados&editar=<?php echo $linha['id_franqueado']?>">
                        <span style="color: GreenYellow;">
                          <i class="fa fa-pencil" aria-hidden="true"></i>
                        </span>
                      </a>
                    </td>
                    <td>
                      <a href="../actions/del_franqueados.php?id_franqueado=<?php echo $linha['id_franqueado'];?>">
                        <span style="color: Red;">
                          <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </span>
                      </a>
                    </td>
                    <?php
                      }
                    ?>
                  </tdbody>
                  <!--  
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Mark</td>
                      <td>Otto</td>
                      <td>@mdo</td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Jacob</td>
                      <td>Thornton</td>
                      <td>@fat</td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>Larry</td>
                      <td>the Bird</td>
                      <td>@twitter</td>
                    </tr>
                    <tr>
                      <td>4</td>
                      <td>Jacob</td>
                      <td>Thornton</td>
                      <td>@fat</td>
                    </tr>
                  </tbody>-->
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>

    <!-- ============================================================================ -->
    <!--Tabela com pesquisa-->

    <!-- Essential javascripts for application to work-->
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="../js/plugins/pace.min.js"></script>
    <!-- Page specific javascripts-->
    <!-- Data table plugin-->
    <script type="text/javascript" src="../js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/plugins/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>

    <!-- ============================================================================ -->

    <!-- Essential javascripts for application to work-->
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="../js/plugins/pace.min.js"></script>
    
    <!-- Page specific javascripts-->
    <!-- Google analytics script-->
    <script type="text/javascript">
      if(document.location.hostname == 'pratikborsadiya.in') {
      	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      	ga('create', 'UA-72504830-1', 'auto');
      	ga('send', 'pageview');
      }
    </script>
  </body>
</html>