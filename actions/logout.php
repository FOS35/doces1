<?php
	session_start();

	unset($_SESSION['login']);
	unset($_SESSION['email']);

	header('location:../view/index.php?pagina=login');
?>